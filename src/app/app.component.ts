import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Task';
  info = {
    id : false,
    employeeName : ''
  };
  data = [];
  tableData;
  add() {
    this.data.push({ ...this.info });
    this.tableData = this.data;
    this.info = {
      id: false,
      employeeName: ''
    };
  }
  delete(i) {
    this.tableData.splice( i , 1);
  }
  all() {
    this.tableData = this.data;
  }
  active() {
    this.tableData = [...this.data.filter(x => x.id !== true)];
  }
  complete() {
    this.tableData = [...this.data.filter(x => x.id === true)];
  }
}
